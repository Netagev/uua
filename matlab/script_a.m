close all;
clear;
clc;

%%


Fs = 16000;  
N=4096;
t  = linspace(0, N*Fs, N);                        % One Second Time Vector
w = 2*pi*1000;                                  % Radian Value To Create 1kHz Tone
s = sin(w*t);                                   % Create Tone
                                 
audiowrite('audiofile.wav', s, Fs);
