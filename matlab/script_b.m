close all;
clear;
clc;


%%

[samp,Fs] = audioread('audiofile.wav');

FFT=abs(fft(samp));
plot(FFT);
[M,I]=max(FFT);
[peaks,W]  = findpeaks(FFT,Fs);
N=size(samp,1);
freq=(N/2)-I;