//
// Created by Ofir Ben Yosef on 08/09/2021.
//
#include "stdio.h"
#include "signal-processing.h"
#include <iostream>
#include <math.h>
#include <signal.h>
#include "fft.h"
#include "parameters.h"
// define
#define BUFCOUNT  8192

using namespace std;
void tone_detection(short adin[2 * BLOCK_SIZE], double tones[]);
void copy_array(short* in, short* out, int length) {
    double tones[4] = {0};
    for (int j = 0; j < length; j++) {
        out[j] = in[j];
    }

    tone_detection(in,tones);
    int max = 0;
    for(int i=0;i<4;i++){
      if(max<=tones[i]) {
          max = tones[i];
      }
      else{
          max = max;
      }
    }


}
int copy_array2(short* in, short* out, int length) {
    double tones[4] = {0};
    for (int j = 0; j < length; j++) {
        out[j] = in[j];
    }

    tone_detection(in,tones);
    int max = 0;
    for(int i=0;i<4;i++){
        if(max<=tones[i]) {
            max = tones[i];
        }
        else{
            max = max;
        }
    }
    return max;

}
void singen1(short l[], short r[], short sin_L, short sin_R, float fs) {
    short i=0;
    float stepL=0, stepR=0;
    static float phaseL=0.0, phaseR=0.0;
    short len = 8192;

    if(fs==0){
        for(i=0;i<len;i++){
            l[i] =0;
        }
    }
    else {

        stepL = ((float) sin_L) / fs;
        stepR = ((float) sin_R) / fs;

        for (i = 0; i < len / 2; i++) {
            l[2 * i] = (short) (Q14 * sin(phaseL));
            l[2 * i + 1] = (short) (Q14 * sin(phaseR + PI /
                                                       2));  //change to sin(phaseR+PI/2)) to creat a circle

            phaseL += stepL * 2 * PI;
            if (phaseL > (2 * PI)) phaseL -= 2 * PI;
            phaseR += stepR * 2 * PI;
            if (phaseR > (2 * PI)) phaseR -= 2 * PI;
        }
    }
}
short sintbl[160] =
        {0, 643, 1285, 1926, 2563,
         3196, 3825, 4447, 5063, 5670,6270, 6859, 7438, 8005, 8560,
         9102, 9630, 10143, 10640, 11121,11585, 12030, 12458, 12866, 13254,
         13622, 13969, 14294, 14597, 14878,15136, 15370, 15581, 15768, 15930,
         16068, 16181, 16269, 16332, 16370,16383, 16370, 16332, 16269, 16181,
         16068, 15930, 15768, 15581, 15370,15136, 14878, 14597, 14294, 13969,
         13622, 13254, 12866, 12458, 12030,11585, 11121, 10640, 10143, 9630,
         9102, 8560, 8005, 7438, 6859,6270, 5670, 5063, 4447, 3825,
         3196, 2563, 1926, 1285, 643,0, -643, -1285, -1926, -2563,
         -3196, -3825, -4447, -5063, -5670,-6270, -6859, -7438, -8005, -8560,
         -9102, -9630, -10143, -10640, -11121,-11585, -12030, -12458, -12866, -13254,
         -13622, -13969, -14294, -14597, -14878,-15136, -15370, -15581, -15768, -15930,
         -16068, -16181, -16269, -16332, -16370,-16383, -16370, -16332, -16269, -16181,
         -16068, -15930, -15768, -15581, -15370,-15136, -14878, -14597, -14294, -13969,
         -13622, -13254, -12866, -12458, -12030,-11585, -11121, -10640, -10143, -9630,
         -9102, -8560, -8005, -7438, -6859,-6270, -5670, -5063, -4447, -3825,
         -3196, -2563, -1926, -1285, -643};
void singen_from_table(short out[],short freq, float Fs) {
    int i;
    static short step, jump=0;
    step=freq;
    int length = 8192;
    for (i=0; i<(length/2);i++){

        jump= jump + step;

        if(jump>=Fs) jump=jump-Fs;
        out[2*i+1]= sintbl[jump/100];
        out[2*i]=sintbl[jump/100];
    }
}

void tone_detection(short adin[2 * BLOCK_SIZE], double tones[]) {
    /* @brief    finds the tones whith the largest amplitude within a given slice
       @param    adin[]: the audio input
                 tones[]: the detected tones from the slice
                 slice: the part of the input audio array we detect from

    */
    int j, i,k, s=0;
    double amplitude[FFT_SIZE], tones_amp[DETECTED_TONES] = { 0 };
    double temp = 0, tempidx = 0;

    double SA[FFT_SIZE][2] = { 0 };

    for (j = 0; j < MIN_FFT_SIZE; j++) {
        SA[j][0] = ((double) adin[2*MIN_FFT_SIZE+2 * j]);
        SA[j][1] = 0;
    }

    gpfft(SA, 0);

    for (i = 0; i < DETECTED_TONES; i++) {
        tones[i] = END_TONE*SAMPLING_FREQ / 4*FFT_SIZE;
    }
    for (k = START_TONE/JUMP; k < START_TONE/JUMP+(END_TONE-START_TONE)/(DETECTED_TONES*JUMP) ; k++) {
        for (i = 0; i < DETECTED_TONES; i++) {
            j=(k+i*DETECTED_TONES)*JUMP;
            amplitude[j] = SA[j][0] * SA[j][0] + SA[j][1] * SA[j][1];
            if ((amplitude[j] > tones_amp[i])&& (amplitude[j] > 0.01)){
                tones_amp[i] = amplitude[j];
                tones[i] = double(j) * SAMPLING_FREQ / FFT_SIZE;
                //printf("\n tones[%d]=%f" ,i,tones[i] );
            }
        }
    }
//    if (amplitude[j] > tones_amp[0]) {
//            tones_amp[DETECTED_TONES - 1] = amplitude[j];
//            tones[DETECTED_TONES - 1] = double(j) * SAMPLING_FREQ / FFT_SIZE;
//            for (i = 1; i < DETECTED_TONES; i++) {
//                if (tones_amp[DETECTED_TONES - i] > tones_amp[DETECTED_TONES - 1 - i]) {
//                    temp = tones_amp[DETECTED_TONES - 1 - i];
//                    tones_amp[DETECTED_TONES - 1 - i] = tones_amp[DETECTED_TONES - i];
//                    tones_amp[DETECTED_TONES - i] = temp;
//                    tempidx = tones[DETECTED_TONES - 1 - i];
//                    tones[DETECTED_TONES - 1 - i] = tones[DETECTED_TONES - i];
//                    tones[DETECTED_TONES - i] = tempidx;
//                }
//            }


    //intf("\n s= %d", s);
}


