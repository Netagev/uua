#include <jni.h>
#include <string>
#include <iostream>
#include <sstream>
#include "parameters.h"
#include "signal-processing.h"
#include "fft.h"
using namespace std;


void copy_array(jshort *pInt, jshort *pInt1, int length);


//void Tone(jshort *pInt, jshort *i, int length);

void singen1(jshort *pInt, jshort *pInt1, jshort l, jshort r, jfloat d);

extern "C" JNIEXPORT jstring JNICALL
Java_com_example_myapplication_MainActivity_stringFromJNI(
        JNIEnv* env,
        jobject /* this */) {
    std::string hello = "AudioProcessing()";
    return env->NewStringUTF(hello.c_str());
}

void singen1(jshort *pInt, jshort *pInt1, jshort l, jshort r, jfloat d);


extern "C"
JNIEXPORT void JNICALL
Java_com_example_myapplication_MainActivity_AudioProcessing(JNIEnv *env, jobject thiz,
                                                            jshortArray audio_data_in,
                                                            jshortArray audio_data_out) {
    jshort *audioDataIn;
    jshort *audioDataOut;
    audioDataIn = env->GetShortArrayElements(audio_data_in, NULL);
    audioDataOut = env->GetShortArrayElements(audio_data_out, NULL);
    int length = env->GetArrayLength(audio_data_in);
    // TODO: implement AudioProcessing()
     copy_array(audioDataIn, audioDataOut, length);
    //tone_detection(audioDataIn,audioDataOut );
    env->ReleaseShortArrayElements(audio_data_in, audioDataIn, 0);
    env->ReleaseShortArrayElements(audio_data_out, audioDataOut, 0);
}




extern "C"
JNIEXPORT void JNICALL
Java_com_example_myapplication_MainActivity_CreateTone(JNIEnv *env, jobject thiz,
                                                       jshortArray audio_data_out, jshort sin__l,
                                                       jint sin__r, jfloat fs) {
    jshort *audioDataOut_R;
    jshort *audioDataOut_L;
    audioDataOut_R = env->GetShortArrayElements(audio_data_out, NULL);
    audioDataOut_L = env->GetShortArrayElements(audio_data_out, NULL);
    // TODO: implement CreateTone()
    singen1(audioDataOut_L, audioDataOut_R, sin__l, sin__r, fs);
    env->ReleaseShortArrayElements(audio_data_out, audioDataOut_R, 0);
    env->ReleaseShortArrayElements(audio_data_out, audioDataOut_L, 0);
}
void singen_from_table(jshort *pInt, jshort freq, jfloat fs) ;




/*
extern "C"
JNIEXPORT void JNICALL
Java_com_example_myapplication_MainActivity_singen_1from_1table(JNIEnv *env, jobject thiz,
                                                                jshortArray out, jshort freq,
                                                                jfloat fs) {
    jshort *audioDataOut;
    audioDataOut = env->GetShortArrayElements(out, NULL);
    // TODO: implement CreateTone()
    singen_from_table(audioDataOut,freq, fs);
    env->ReleaseShortArrayElements(out, audioDataOut, 0);

}
*/
int copy_array2(jshort *pInt, jshort *pInt1, int length);

extern "C"
JNIEXPORT void JNICALL
Java_com_example_myapplication_MainActivity_singen_1from_1table(JNIEnv *env, jobject thiz,
                                                                jshortArray out, jshort freq,
                                                                jfloat fs) {
    jshort *audioDataOut;
    audioDataOut = env->GetShortArrayElements(out, NULL);
    singen_from_table(audioDataOut,freq, fs);
    env->ReleaseShortArrayElements(out, audioDataOut, 0);
}

int copy_array2(jshort *pInt, jshort *pInt1, int length) ;


extern "C"
JNIEXPORT jint JNICALL
Java_com_example_myapplication_MainActivity_AudioProcessing2(JNIEnv *env, jobject thiz,
                                                             jshortArray audio_data_in,
                                                             jshortArray audio_data_out) {
        jshort *audioDataIn;
        jshort *audioDataOut;
        audioDataIn = env->GetShortArrayElements(audio_data_in, NULL);
        audioDataOut = env->GetShortArrayElements(audio_data_out, NULL);
        int length = env->GetArrayLength(audio_data_in);
        // TODO: implement AudioProcessing()
        int tone = copy_array2(audioDataIn, audioDataOut, length);
        env->ReleaseShortArrayElements(audio_data_in, audioDataIn, 0);
        env->ReleaseShortArrayElements(audio_data_out, audioDataOut, 0);
    return tone;
}