package com.example.myapplication;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Process;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.myapplication.databinding.ActivityMainBinding;
//import static android.Manifest.permission.RECORD_AUDIO;
//import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class MainActivity<audioDataOut> extends AppCompatActivity {
    private static final int INPUT_CHANNELS = 2, SAMPLING_RATE = 16000, BUFFER_SIZE = 8192;
    public static final int REQUEST_AUDIO_PERMISSION_CODE = 1;
    private static final String WRITE_EXTERNAL_STORAGE = "android.permission.WRITE_EXTERNAL_STORAGE";
    private static final String RECORD_AUDIO = "android.permission.RECORD_AUDIO";
    static int inputSampleRate = SAMPLING_RATE;
    static AudioRecord mAudioRecord;
    static AudioTrack audioTrack;
    static short[] mSamplesShort = new short[BUFFER_SIZE / 2 * INPUT_CHANNELS];
    static short[] mSamplesShortOut = new short[BUFFER_SIZE];

   int freq = 0;

   private EditText freqInput;
   private TextView freqResult;
   private Button submitButton;
   private Switch OnOffS;

    // Used to load the 'myapplication' library on application startup.
    static {
        System.loadLibrary("myapplication");
    }

    private ActivityMainBinding binding;
    private AudioManager m_amAudioManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        freqInput =  findViewById(R.id.freqInput);
        freqResult = findViewById(R.id.freq_result);
        submitButton = findViewById(R.id.submitButtonInput);
        OnOffS = findViewById(R.id.on_off);
        while (!CheckPermissions()) {
            Log.w("MainActivity/", "Requesting Audio Permissions");
            RequestPermissions();
        }
        if (CheckPermissions()) {
            Log.w("MainActivity/", "Audio Permissions Granted");
            RunAudioThread();
        } else {
            Log.w("MainActivity/", "Audio Permissions Denied");
        }
        submitButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                freq = Integer.parseInt(freqInput.getText().toString());
                showToast(String.valueOf(freq));
                freqResult.setText(String.valueOf(freq));

            }
        });
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // Example of a call to a native method
        TextView tv = binding.sampleText;
        tv.setText(stringFromJNI());

    }
    private void showToast(String text){
        Toast.makeText(MainActivity.this,text,Toast.LENGTH_SHORT).show();
    }
    private boolean CheckPermissions() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(),
                WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED && result1 ==
                PackageManager.PERMISSION_GRANTED;
    }

    private void RequestPermissions() {
        ActivityCompat.requestPermissions(MainActivity.this, new String[]{RECORD_AUDIO,
                WRITE_EXTERNAL_STORAGE}, REQUEST_AUDIO_PERMISSION_CODE);
    }

    public void RunAudioThread() {
        Log.w("Main/RunAudioThread", "Starting");
        new Thread(new Runnable() {
            @Override
            public void run() {
                Process.setThreadPriority(Process.THREAD_PRIORITY_URGENT_AUDIO);
                StartAudioIO();
                while (mAudioRecord != null && mAudioRecord.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING) {
                    freqInput = findViewById(R.id.freqInput);
                    freqResult = findViewById(R.id.freq_result);
                    submitButton = findViewById(R.id.submitButtonInput);
                    OnOffS = findViewById(R.id.on_off);

                    Boolean switchState = OnOffS.isChecked();
                    if (switchState) {
                        submitButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                freq = Integer.parseInt(freqInput.getText().toString());
                                freqResult.setText(String.valueOf(freq));

                            }
                        });
                    }
                    else{
                        freq = 0;
                        freqResult.setText(String.valueOf(freq));

                    }
                        mAudioRecord.read(mSamplesShort, 0, (BUFFER_SIZE / 2) * INPUT_CHANNELS);
                         freq =  AudioProcessing2(mSamplesShort, mSamplesShortOut);
                        //CreateTone(mSamplesShortOut, (short) SAMPLING_RATE, inputSampleRate, freq);
                        //singen_from_table(mSamplesShortOut, (short) freq, SAMPLING_RATE);
                        freqResult.setText(String.valueOf(freq));
                        audioTrack.write(mSamplesShortOut, 0, (BUFFER_SIZE / 2) * INPUT_CHANNELS);
                    }

                Log.w("Main/RunAudioThread", "End");
            }
        }).start();
    }

    private void StartAudioIO() {
        Log.w("Main/StartAudioIO", "Starting");
        int inputChannel = INPUT_CHANNELS;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mAudioRecord = new AudioRecord(MediaRecorder.AudioSource.CAMCORDER, inputSampleRate, inputChannel == 1 ? AudioFormat.CHANNEL_IN_MONO :
                AudioFormat.CHANNEL_IN_STEREO, AudioFormat.ENCODING_PCM_16BIT, BUFFER_SIZE * inputChannel);
            mAudioRecord.startRecording();
            m_amAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, inputSampleRate, AudioFormat.CHANNEL_IN_STEREO, AudioFormat.ENCODING_PCM_16BIT, BUFFER_SIZE * 2, AudioTrack.MODE_STREAM);
            audioTrack.setStereoVolume((float)0.9, (float)0.9);
            audioTrack.play();
    }
       /**
     * A native method that is implemented by the 'myapplication' native library,
     * which is packaged with this application.
     */

    public native String stringFromJNI();
    public native void AudioProcessing(short[] audioDataIn, short[] audioDataOut);
    public native void CreateTone(short[] audioDataOut, short sin_L, int sin_R, float FS);
    public native void singen_from_table(short[] out,short freq, float Fs);
    public native int AudioProcessing2(short[] audioDataIn,  short[] audioDataOut);


}